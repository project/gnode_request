<?php

/**
 * @file
 * Views related hooks.
 */

/**
 * Implements hook_views_data().
 */
function gnode_request_views_data() {
  $data['group_content']['approve_gnode_request'] = [
    'field' => [
      'title' => t('Approve'),
      'help' => t('Provide a link to approve request.'),
      'id' => 'approve_gnode_request',
    ],
  ];

  $data['group_content']['reject_gnode_request'] = [
    'field' => [
      'title' => t('Reject'),
      'help' => t('Provide a link to reject request.'),
      'id' => 'reject_gnode_request',
    ],
  ];

  return $data;
}
