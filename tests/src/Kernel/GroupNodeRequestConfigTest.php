<?php

namespace Drupal\Tests\gnode_request\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests that all config provided by this module passes validation.
 *
 * @group gnode_request
 */
class GroupNodeRequestConfigTest extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'views',
    'group',
    'options',
    'entity',
    'node',
    'variationcache',
    'state_machine',
    'gnode_request',
  ];

  /**
   * Tests that the module's config installs properly.
   */
  public function testConfig() {
    $this->installConfig(['gnode_request']);
  }

}
