<?php

namespace Drupal\Tests\gnode_request\Kernel;

use Drupal\Tests\group\Kernel\GroupKernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests access permissions interaction with other node group membership.
 *
 * @group gnode_request
 */
class GroupNodeRequestAccessTest extends GroupKernelTestBase {

  use ContentTypeCreationTrait;
  use NodeCreationTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = ['gnode', 'gnode_request', 'state_machine', 'node', 'field', 'system'];

  /**
   * The node storage to use in testing.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $storage;

  /**
   * The access control handler to use in testing.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $accessControlHandler;

  /**
   * The first group type to use in testing.
   *
   * @var \Drupal\group\Entity\GroupTypeInterface
   */
  protected $groupTypeA;

  /**
   * The second group type to use in testing.
   *
   * @var \Drupal\group\Entity\GroupTypeInterface
   */
  protected $groupTypeB;

  /**
   * The required permissions for our node checks.
   *
   * @var string[]
   */
  protected $permissions = [
    'access content',
    'view own unpublished content',
    'edit own page content',
    'edit any page content',
    'delete any page content',
    'delete any page content',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installConfig(['user', 'gnode_request', 'state_machine', 'gnode', 'node', 'filter']);
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('node');

    $this->storage = $this->entityTypeManager->getStorage('node');
    $this->accessControlHandler = $this->entityTypeManager->getAccessControlHandler('node');

    $this->createContentType(['type' => 'page']);
    $this->createContentType(['type' => 'article']);

    $this->groupTypeA = $this->createGroupType(['id' => 'foo', 'creator_membership' => FALSE]);
    $this->groupTypeB = $this->createGroupType(['id' => 'bar', 'creator_membership' => FALSE]);

    /** @var \Drupal\group\Entity\Storage\GroupContentTypeStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('group_content_type');
    $storage->save($storage->createFromPlugin($this->groupTypeA, 'group_node:page'));
    $storage->save($storage->createFromPlugin($this->groupTypeA, 'group_node:article'));
    $storage->save($storage->createFromPlugin($this->groupTypeB, 'group_node:page'));
    $storage->save($storage->createFromPlugin($this->groupTypeB, 'group_node:article'));

    $storage->save($storage->createFromPlugin($this->groupTypeA, 'group_node_request:page'));
    $storage->save($storage->createFromPlugin($this->groupTypeA, 'group_node_request:article'));
    $storage->save($storage->createFromPlugin($this->groupTypeB, 'group_node_request:page'));
    $storage->save($storage->createFromPlugin($this->groupTypeB, 'group_node_request:article'));

    $this->setCurrentUser($this->createUser([], $this->permissions));
  }

  /**
   * Tests that grouped request maintains standard published access.
   */
  public function testMemberGroupPublishedAccessWithoutPermission() {
    $node_1 = $this->createNode(['type' => 'page', 'uid' => 0]);

    $this->assertTrue($this->accessControlHandler->access($node_1, 'view'), 'Regular published view access works.');

    $group = $this->createGroup(['type' => $this->groupTypeA->id()]);
    $group->addContent($node_1, 'group_node_request:page');
    $this->assertTrue($this->accessControlHandler->access($node_1, 'view'), 'Can view published request without membership.');

    $group->addMember($this->getCurrentUser());
    $this->assertTrue($this->accessControlHandler->access($node_1, 'view'), 'Can view published request with membership.');
  }

  /**
   * Tests that grouped request maintains standard published access.
   */
  public function testMemberGroupUnublishedAccessWithoutPermission() {
    $node_1 = $this->createNode(['type' => 'page', 'uid' => 0, 'status' => 0]);

    $this->assertFalse($this->accessControlHandler->access($node_1, 'view'), 'Regular unpublished view access works.');

    $group = $this->createGroup(['type' => $this->groupTypeA->id()]);
    $group->addContent($node_1, 'group_node_request:page');
    $this->assertFalse($this->accessControlHandler->access($node_1, 'view'), 'Cannot view unpublished request without membership.');

    $group->addMember($this->getCurrentUser());
    $this->assertFalse($this->accessControlHandler->access($node_1, 'view'), 'Cannot view unpublished request with membership.');
  }

  /**
   * Tests that group request adds access to unpublished with permisison.
   */
  public function testMemberGroupUnpublishedAccessWithPermission() {
    $node_1 = $this->createNode(['type' => 'page', 'uid' => 0, 'status' => 0]);

    $this->groupTypeA->getMemberRole()->grantPermission('view any unpublished group_node_request:page entity')->save();
    $group = $this->createGroup(['type' => $this->groupTypeA->id()]);
    $group->addContent($node_1, 'group_node_request:page');

    // Can't do this because GroupPermissionsHashGenerator::generateHash stores
    // the hash including group membership used in the cache tag per uid. It's
    // not a static that can be externally refreshed, so only a new request.
    //
    // $this->assertFalse($this->accessControlHandler->access($node_1, 'view'), 'Regular unpublished view access works.');

    $group->addMember($this->getCurrentUser());
    $this->assertTrue($this->accessControlHandler->access($node_1, 'view'), 'Member permission view unpublished view access works.');
  }

  // @codingStandardsIgnoreStart
  /**
   * Tests group request access to unpublished also in another group.
   */
  //https://www.drupal.org/project/group/issues/3246202
  //
  // public function testMemberTwoGroupUnpublishedAccessWithPermission() {
  //   $node_1 = $this->createNode(['type' => 'page', 'uid' => 0, 'status' => 0]);
  //
  //   $this->groupTypeA->getMemberRole()->grantPermission('view any unpublished group_node_request:page entity')->save();
  //   $group_requested = $this->createGroup(['type' => $this->groupTypeA->id()]);
  //   $group_requested->addContent($node_1, 'group_node_request:page');
  //   $group_in = $this->createGroup(['type' => $this->groupTypeB->id()]);
  //   $group_in->addContent($node_1, 'group_node:page');
  //
  //   $group_requested->addMember($this->getCurrentUser());
  //   $this->assertTrue($this->accessControlHandler->access($node_1, 'view'), 'Member permission view unpublished view access works.');
  // }
  // @codingStandardsIgnoreEnd

}
