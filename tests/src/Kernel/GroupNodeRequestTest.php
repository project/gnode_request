<?php

namespace Drupal\Tests\gnode_request\Kernel;

use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\gnode_request\Plugin\GroupContentEnabler\GroupNodeRequest;
use Drupal\Tests\group\Kernel\GroupKernelTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests the general behavior of group entities.
 *
 * @coversDefaultClass \Drupal\gnode_request\Entity\GroupNodeRequest
 * @group gnode_request
 */
class GroupNodeRequestTest extends GroupKernelTestBase {

  use ContentTypeCreationTrait;
  use NodeCreationTrait;

  /**
   * Membership request manager.
   *
   * @var \Drupal\gnode_request\MembershipRequestManager
   */
  protected $membershipRequestManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The group we will use to test methods on.
   *
   * @var \Drupal\group\Entity\Group
   */
  protected $group;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['gnode', 'gnode_request', 'state_machine', 'node', 'field', 'system'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installSchema('node', 'node_access');
    $this->installConfig(['gnode_request', 'state_machine', 'node', 'filter']);

    $this->membershipRequestManager = $this->container->get('gnode_request.membership_request_manager');
    $this->entityTypeManager = $this->container->get('entity_type.manager');

    $this->group = $this->createGroup();

    $this->groupContentType = $this->createContentType(['type' => 'page']);
    $this->otherContentType = $this->createContentType();

    // Enable group membership request group content plugin.
    $group_content_type_storage = $this->entityTypeManager->getStorage('group_content_type');
    $group_content_type_storage->save($group_content_type_storage->createFromPlugin($this->group->getGroupType(), 'group_node:page'));
    $group_content_type_storage->save($group_content_type_storage->createFromPlugin($this->group->getGroupType(), 'group_node_request:page'));
  }

  /**
   * Test the creation of a request when node is the member.
   */
  public function testAddRequestForExistingContent() {
    $node = $this->createNode();
    $this->group->addContent($node, 'group_node:page');

    $this->expectExceptionMessage('This node is already a member of the group');
    $this->membershipRequestManager->create($this->group, $node);
  }

  /**
   * Test validation of the request when node already is a member.
   */
  public function testRequestWithMemberAsContent() {
    $node = $this->createNode();

    $group_node_request = $this->membershipRequestManager->create($this->group, $node);
    $this->group->addContent($node, 'group_node:page');

    $violations = $group_node_request->validate();
    $this->assertNotEquals(count($violations), 0);

    $messages = [];
    foreach ($violations as $violation) {
      $messages[] = $violation->getMessage()->getUntranslatedString();
    }

    $this->assertTrue(in_array('Node "@title" is already a member of group', $messages), 'The validation of group membership request is found.');
  }

  /**
   * Test accepting a membership.
   */
  public function testNodeRequestAccept() {
    $node = $this->createNode();
    $group_node_request = $this->membershipRequestManager->create($this->group, $node);
    $group_node_request->save();

    // Check status.
    $this->assertEquals($group_node_request->gnode_request_status->value, GroupNodeRequest::REQUEST_PENDING);
    // Check group relations.
    $group_relations = $this->group->getContentByEntityId('group_node:' . $node->getType(), $node->id());
    $this->assertEmpty($group_relations);

    // Approve. 
    $this->membershipRequestManager->approve($group_node_request);

    // Check status.
    $this->assertEquals($group_node_request->gnode_request_status->value, GroupNodeRequest::REQUEST_APPROVED);
    // Check group relations.
    $group_relations = $this->group->getContentByEntityId('group_node:' . $node->getType(), $node->id());
    $this->assertCount(1, $group_relations);
  }

  /**
   * Test reject a membership.
   */
  public function testNodeRequestReject() {
    $node = $this->createNode();
    $group_node_request = $this->membershipRequestManager->create($this->group, $node);
    $group_node_request->save();

    // Check status.
    $this->assertEquals($group_node_request->gnode_request_status->value, GroupNodeRequest::REQUEST_PENDING);
    // Check group relations.
    $group_relations = $this->group->getContentByEntityId('group_node:' . $node->getType(), $node->id());
    $this->assertEmpty($group_relations);

    // Reject.
    $this->membershipRequestManager->reject($group_node_request);

    // Check status.
    $this->assertEquals($group_node_request->gnode_request_status->value, GroupNodeRequest::REQUEST_REJECTED);
    // Check group relations.
    $group_relations = $this->group->getContentByEntityId('group_node:' . $node->getType(), $node->id());
    $this->assertEmpty($group_relations);
  }

  /**
   * Test acceptance by direct add.
   */
  public function testNodeDirectAccept() {
    $node = $this->createNode();
    $group_node_request = $this->membershipRequestManager->create($this->group, $node);
    $group_node_request->save();

    // Check status.
    $this->assertEquals(GroupNodeRequest::REQUEST_PENDING, $group_node_request->gnode_request_status->value);
    // Check group relations.
    $group_relations = $this->group->getContentByEntityId('group_node:' . $node->getType(), $node->id());
    $this->assertEmpty($group_relations);

    // Approve. 
    $this->group->addContent($node, 'group_node:' . $node->getType());
    $group_node_request = $this->membershipRequestManager->getMembershipRequest($node, $this->group);

    // Check status.
    $this->assertEquals(GroupNodeRequest::REQUEST_APPROVED, $group_node_request->gnode_request_status->value);
    // Check group relations.
    $group_relations = $this->group->getContentByEntityId('group_node:' . $node->getType(), $node->id());
    $this->assertCount(1, $group_relations);
  }

  /**
   * Test deletion of group membership request after node deletion.
   */
  public function testNodeDeletion() {
    $node = $this->createNode();
    $group_node_request = $this->membershipRequestManager->create($this->group, $node);
    $group_node_request->save();

    $membership_request = $this->membershipRequestManager->getMembershipRequest($node, $this->group);
    $this->assertEquals($membership_request->id(), $group_node_request->id());

    $this->entityTypeManager->getStorage('node')->delete([$node]);

    $membership_request = $this->membershipRequestManager->getMembershipRequest($node, $this->group);
    $this->assertNull($membership_request);
  }

  /**
   * Test deletion of group membership request after group membership deletion.
   */
  public function testGroupNodeDeletion() {
    $node = $this->createNode();
    $group_node_request = $this->membershipRequestManager->create($this->group, $node);
    $group_node_request->save();

    $this->group->addContent($node, 'group_node:' . $node->getType());
    $group_relations = $this->group->getContentByEntityId('group_node:' . $node->getType(), $node->id());
    $group_node = reset($group_relations);
    $group_node->delete();

    $membership_request = $this->membershipRequestManager->getMembershipRequest($node, $this->group);
    $this->assertNull($membership_request);
  }

}
