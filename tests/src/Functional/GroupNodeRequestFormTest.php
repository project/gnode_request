<?php

namespace Drupal\Tests\gnode_request\Functional;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\group\Entity\GroupContent;
use Drupal\Tests\group\Functional\GroupBrowserTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests the behavior of the group request forms.
 *
 * @group gnode_request
 */
class GroupNodeRequestFormTest extends GroupBrowserTestBase {

  use StringTranslationTrait;
  use ContentTypeCreationTrait;
  use NodeCreationTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'group',
    'gnode',
    'group_test_config',
    'state_machine',
    'gnode_request',
  ];

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Membership request manager.
   *
   * @var \Drupal\gnode_request\MembershipRequestManager
   */
  protected $membershipRequestManager;

  /**
   * The group we will use to test methods on.
   *
   * @var \Drupal\group\Entity\Group
   */
  protected $group;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->membershipRequestManager = $this->container->get('gnode_request.membership_request_manager');
    $this->entityTypeManager = $this->container->get('entity_type.manager');

    $this->group = $this->createGroup();
    $this->groupContentType = $this->createContentType(['type' => 'page']);
    $this->otherContentType = $this->createContentType();

    // Enable group membership request group content plugin.
    $config = [
      'group_cardinality' => 0,
      'entity_cardinality' => 1,
    ];
    $group_content_type_storage = $this->entityTypeManager->getStorage('group_content_type');
    $group_content_type_storage->save($group_content_type_storage->createFromPlugin($this->group->getGroupType(), 'group_node:page'));
    $group_content_type_storage->save($group_content_type_storage->createFromPlugin($this->group->getGroupType(), 'group_node_request:page', $config));

    // Add a text field to the group content type.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_test_text',
      'entity_type' => 'group_content',
      'type' => 'text',
    ]);
    $field_storage->save();

    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $this->group
        ->getGroupType()
        ->getContentPlugin('group_node_request:page')
        ->getContentTypeConfigId(),
      'label' => 'String long',
    ])->save();

    EntityFormDisplay::create([
      'targetEntityType' => 'group_content',
      'bundle' => $this->group
        ->getGroupType()
        ->getContentPlugin('group_node_request:page')
        ->getContentTypeConfigId(),
      'mode' => 'default',
      'status' => TRUE,
    ])->setComponent('field_test_text', ['type' => 'text_textfield'])->enable()->save();

    // Add permissions to the creator of the group.
    $role = $this->group->getGroupType()->getMemberRole();
    $role->grantPermissions(['administer group node requests']);
    $role->save();
  }

  /**
   * Tests approval form.
   */
  public function testApprovalForm() {
    $node = $this->createNode();
    $group_membership_request = $this->membershipRequestManager->create($this->group, $node);
    $group_membership_request->save();

    $this->drupalGet("/group/{$this->group->id()}/content/{$group_membership_request->id()}/approve");
    $this->assertSession()->statusCodeEquals(200);

    $submit_button = 'Approve';
    $this->assertSession()->buttonExists($submit_button);
    $this->assertSession()->linkExists('Cancel');
    $this->assertSession()->linkByHrefExists($this->group->toUrl()->toString());
    $this->assertSession()->pageTextContains(strip_tags($this->t('Are you sure you want to approve a request for %node?', ['%node' => $node->label()])->render()));

    $this->submitForm([], $submit_button);
    $this->assertSession()->pageTextContains($this->t('Membership request approved'));

    $group_membership = $this->group->getContentByEntityId('group_node:page', $node->id());
    $this->assertTrue(reset($group_membership) instanceof GroupContent, 'Group membership has been successfully created.');
  }

  /**
   * Tests reject form.
   */
  public function testRejectForm() {
    $node = $this->createNode();
    $group_membership_request = $this->membershipRequestManager->create($this->group, $node);
    $group_membership_request->save();

    $this->drupalGet("/group/{$this->group->id()}/content/{$group_membership_request->id()}/reject");
    $this->assertSession()->statusCodeEquals(200);

    $submit_button = 'Reject';
    $this->assertSession()->buttonExists($submit_button);
    $this->assertSession()->linkExists('Cancel');
    $this->assertSession()->linkByHrefExists($this->group->toUrl()->toString());
    $this->assertSession()->pageTextContains(strip_tags($this->t('Are you sure you want to reject a request for %node?', ['%node' => $node->label()])->render()));

    $this->submitForm([], $submit_button);
    $this->assertSession()->pageTextContains($this->t('Membership request rejected'));

    $group_membership = $this->group->getContentByEntityId('group_node:page', $node->id());
    $this->assertEmpty($group_membership, 'Group membership was not found.');
  }

}
