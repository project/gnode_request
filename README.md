# INTRODUCTION

Group Node Request - This module extends [group](https://www.drupal.org/project/group)
and it's group node module to allows users to request a node become part of a group.

# REQUIREMENTS

This module requires the following modules:

* [Group](https://www.drupal.org/project/group)
* [State machine](https://www.drupal.org/project/state_machine)

# INSTALLATION

You can install this module using standard user interface or using cli command

```
drush en gnode_request
```

# CONFIGURATION

 1) Add **Group node request** for the desired content types. These should also
    have **Group node** enabled.

/admin/group/types/manage/[ YOUR_GROUP_TYPE ]/content

 2) Configure permissions as desired. 

/admin/group/types/manage/[ YOUR_GROUP_TYPE ]/permissions

    The **Administer group request proposals** is for the users who will accept
    or reject propsed entities.

    **Entity: Add content item entities** will allow creating new content and
    proposing it the group. **Relation: Add content item entities** will allow
    proposing existing content to the group.

 3) Check /group/[ GROUP ID ]/node_request to see, to approve or
    to reject all pending requests. To access this page
    the current user need to have "**administer group node requests**"
    permission.


#  Development

Request manager helps to handle main operation on requests.
Please use it whenever you need to create, approve or reject a request.
It will trigger all necessary events for you.

```
$request_manager = \Drupal::service('gnode_request.membership_request_manager');
```

1) To add a new request in the code use
```
$request_manager->create($group, $node);
```

2) To approve a new request in the code use
```
$request_manager->approve($group_content_request_membership);
```

3) To reject a new request in the code use
```
$request_manager->reject($group_content_request_membership);
```

4) To get the request fot the user

```
$request_manager->getMembershipRequest($node, $group);
```

# Available events

#### For request's approval

```
group_membership_request.approve.pre_transition

group_membership_request.approve.post_transition
```

#### For request's rejection

```
group_membership_request.reject.pre_transition

group_membership_request.reject.post_transition
```

#### An example of an event subscriber

```
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class MyEventSubscriber implements EventSubscriberInterface {

  public static function getSubscribedEvents() {
    return [
      'group_membership_request.approve.pre_transition' => 'preApprove',
      'group_membership_request.approve.post_transition' => 'postApprove',
      'group_membership_request.reject.pre_transition' => 'preReject',
      'group_membership_request.reject.post_transition' => 'postReject',
    ];
  }

  public function preApprove(WorkflowTransitionEvent $event) {
    $this->setMessage($event, __FUNCTION__);
  }

  public function postApprove(WorkflowTransitionEvent $event) {
    $this->setMessage($event, __FUNCTION__);
  }

  public function preReject(WorkflowTransitionEvent $event) {
    $this->setMessage($event, __FUNCTION__);
  }

  public function postReject(WorkflowTransitionEvent $event) {
    $this->setMessage($event, __FUNCTION__);
  }

  protected function setMessage(WorkflowTransitionEvent $event, $phase) {
    $str = '@entity_label (@field_name)';
    $str .= '- @state_label at @phase (workflow: @workflow).';
    \Drupal::messenger()->addMessage(new TranslatableMarkup($str, [
      '@entity_label' => $event->getEntity()->label(),
      '@field_name' => $event->getFieldName(),
      '@state_label' => $event->getTransition()->getToState()->getLabel(),
      '@workflow' => $event->getWorkflow()->getId(),
      '@phase' => $phase,
    ]));
  }

}

```

# Credits

For the grequest module on which lots of this code is based [Nikolay Lobachev](https://www.drupal.org/u/lobsterr).
