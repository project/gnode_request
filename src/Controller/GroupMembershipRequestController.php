<?php

namespace Drupal\gnode_request\Controller;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\gnode_request\MembershipRequestManager;
use Drupal\group\Entity\GroupContentInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides group membership request route controllers.
 */
class GroupMembershipRequestController extends ControllerBase {

  /**
   * Membership request manager.
   *
   * @var \Drupal\grequest\MembershipRequestManager
   */
  protected $membershipRequestManager;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Group Membership request controller constructor.
   *
   * @param \Drupal\grequest\MembershipRequestManager $membership_request_manager
   *   Membership request manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(MembershipRequestManager $membership_request_manager, AccountInterface $current_user, EntityTypeManagerInterface $entity_type_manager) {
    $this->membershipRequestManager = $membership_request_manager;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('gnode_request.membership_request_manager'),
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Provides the form for approval a group membership.
   *
   * @param \Drupal\group\Entity\GroupContentInterface $group_content
   *   The group content.
   *
   * @return array
   *   A group approval membership form.
   */
  public function approveMembership(GroupContentInterface $group_content) {
    return $this->entityFormBuilder()->getForm($group_content, 'group-node-approve');
  }

  /**
   * Provides the form for rejection a group membership.
   *
   * @param \Drupal\group\Entity\GroupContentInterface $group_content
   *   The group content.
   *
   * @return array
   *   A group rejection membership form.
   */
  public function rejectMembership(GroupContentInterface $group_content) {
    return $this->entityFormBuilder()->getForm($group_content, 'group-node-reject');
  }

}
