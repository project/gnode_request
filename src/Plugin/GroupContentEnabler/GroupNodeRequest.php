<?php

namespace Drupal\gnode_request\Plugin\GroupContentEnabler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupContentInterface;
use Drupal\group\Plugin\GroupContentEnablerBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\NodeType;

/**
 * Provides a content enabler for node membership requests.
 *
 * @GroupContentEnabler(
 *   id = "group_node_request",
 *   label = @Translation("Group node request"),
 *   description = @Translation("Adds node as requested for membership of a group."),
 *   entity_type_id = "node",
 *   pretty_path_key = "request",
 *   reference_label = @Translation("Title"),
 *   entity_access = TRUE,
 *   reference_description = @Translation("The tile of the node you want to make a member"),
 *   deriver = "Drupal\gnode_request\Plugin\GroupContentEnabler\GroupNodeDeriver",
 *   handlers = {
 *     "access" = "Drupal\gnode_request\Plugin\GroupContentAddAccessControlHandler",
 *     "permission_provider" = "Drupal\gnode_request\Plugin\GroupNodeRequestPermissionProvider",
 *   },
 *   admin_permission = "administer group node requests"
 * )
 */
class GroupNodeRequest extends GroupContentEnablerBase {

  /**
   * Transition id for approval.
   */
  const TRANSITION_APPROVE = 'approve';

  /**
   * Transition id for approval.
   */
  const TRANSITION_REJECT = 'reject';

  /**
   * Status field.
   */
  const STATUS_FIELD = 'gnode_request_status';

  /**
   * Request created and waiting for administrator's response.
   */
  const REQUEST_PENDING = 'pending';

  /**
   * Request is approved by administrator.
   */
  const REQUEST_APPROVED = 'approved';

  /**
   * Request is rejected by administrator.
   */
  const REQUEST_REJECTED = 'rejected';

  /**
   * Retrieves the node type this plugin supports.
   *
   * @return \Drupal\node\NodeTypeInterface
   *   The node type this plugin supports.
   */
  protected function getNodeType() {
    return NodeType::load($this->getEntityBundle());
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupOperations(GroupInterface $group) {
    $account = \Drupal::currentUser();
    $plugin_id = $this->getPluginId();
    $type = $this->getEntityBundle();
    $operations = [];

    if ($group->hasPermission("create $plugin_id entity", $account)) {
      $route_params = ['group' => $group->id(), 'plugin_id' => $plugin_id];
      $operations["gnode-request-create-$type"] = [
        'title' => $this->t('Propose new @type', ['@type' => $this->getNodeType()->label()]),
        'url' => new Url('entity.group_content.create_form', $route_params),
        'weight' => 30,
      ];
    }

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function createAccess(GroupInterface $group, AccountInterface $account) {
    return GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'administer group node requests');
  }

  /**
   * {@inheritdoc}
   */
  protected function viewAccess(GroupContentInterface $group_content, AccountInterface $account) {
    return GroupAccessResult::allowedIfHasGroupPermission($group_content->getGroup(), $account, 'administer group node requests');
  }

  /**
   * {@inheritdoc}
   */
  protected function updateAccess(GroupContentInterface $group_content, AccountInterface $account) {
    return GroupAccessResult::allowedIfHasGroupPermission($group_content->getGroup(), $account, 'administer group node requests');
  }

  /**
   * {@inheritdoc}
   */
  protected function deleteAccess(GroupContentInterface $group_content, AccountInterface $account) {
    return GroupAccessResult::allowedIfHasGroupPermission($group_content->getGroup(), $account, 'administer group node requests');
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityReferenceSettings() {
    $settings = parent::getEntityReferenceSettings();
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function postInstall() {
    if (!\Drupal::isConfigSyncing()) {
      $group_content_type_id = $this->getContentTypeConfigId();

      // Add Status field.
      FieldConfig::create([
        'field_storage' => FieldStorageConfig::loadByName('group_content', static::STATUS_FIELD),
        'bundle' => $group_content_type_id,
        'label' => $this->t('Request status'),
        'required' => TRUE,
        'settings' => [
          'workflow' => 'request',
          'workflow_callback' => '',
        ],
      ])->save();

      // Add "Updated by" field, to save reference to
      // user who approved/denied request.
      FieldConfig::create([
        'field_storage' => FieldStorageConfig::loadByName('group_content', 'gnode_request_updated_by'),
        'bundle' => $group_content_type_id,
        'label' => $this->t('Approved/Rejected by'),
        'settings' => [
          'handler' => 'default',
          'target_bundles' => NULL,
        ],
      ])->save();

      // Build the 'default' display ID for both the entity form and view mode.
      $default_display_id = "group_content.$group_content_type_id.default";
      // Build or retrieve the 'default' view mode.
      if (!$view_display = EntityViewDisplay::load($default_display_id)) {
        $view_display = EntityViewDisplay::create([
          'targetEntityType' => 'group_content',
          'bundle' => $group_content_type_id,
          'mode' => 'default',
          'status' => TRUE,
        ]);
      }

      // Assign display settings for the 'default' view mode.
      $view_display
        ->setComponent(static::STATUS_FIELD, [
          'type' => 'list_default',
        ])
        ->setComponent('updated_by', [
          'label' => 'above',
          'type' => 'entity_reference_label',
          'settings' => [
            'link' => 1,
          ],
        ])
        ->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['entity_cardinality'] = 1;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    $dependencies['config'][] = 'node.type.' . $this->getEntityBundle();
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Disable the entity cardinality field as the functionality of this module
    // relies on a cardinality of 1. We don't just hide it, though, to keep a UI
    // that's consistent with other content enabler plugins.
    $info = $this->t("This field has been disabled by the plugin to guarantee the functionality that's expected of it.");
    $form['entity_cardinality']['#disabled'] = TRUE;
    $form['entity_cardinality']['#description'] .= '<br /><em>' . $info . '</em>';

    return $form;
  }

}
