<?php

namespace Drupal\gnode_request\Plugin;

use Drupal\group\Plugin\GroupContentPermissionProvider;

/**
 * Provides group permissions for group content entities.
 */
class GroupNodeRequestPermissionProvider extends GroupContentPermissionProvider {

  /**
   * {@inheritdoc}
   */
  public function getEntityCreatePermission() {
    if ($this->definesEntityPermissions) {
      return "create $this->pluginId entity";
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityDeletePermission($scope = 'any') {
    // Handled by the admin permission.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityViewPermission($scope = 'any') {
    // Handled by the admin permission.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityUpdatePermission($scope = 'any') {
    // Handled by the admin permission.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelationViewPermission($scope = 'any') {
    // Handled by the admin permission.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelationUpdatePermission($scope = 'any') {
    // Handled by the admin permission.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelationDeletePermission($scope = 'any') {
    // Handled by the admin permission.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPermissions() {
    $permissions = parent::buildPermissions();

    if ($name = $this->getEntityCreatePermission()) {
      $permissions[$name] = $this->buildPermission(
        "Entity: Add %entity_type entities",
        'Allows you to create a new %entity_type entity and propose it for membership of the group.'
      );
    }
    if ($name = $this->getRelationCreatePermission()) {
      $permissions[$name] = $this->buildPermission(
        "Relation: Add %entity_type entities",
        'Allows you to propose an existing content item entity to the group.'
      );
    }

    // Update the title to make user friendly.
    $permissions['administer group node requests']['title'] = 'Administer group request proposals';

    return $permissions;
  }

}
