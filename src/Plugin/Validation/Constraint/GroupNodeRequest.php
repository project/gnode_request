<?php

namespace Drupal\gnode_request\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a user a member of the group.
 *
 * @Constraint(
 *   id = "GroupNodeRequest",
 *   label = @Translation("Group node request checks", context = "Validation"),
 *   type = "entity:group_content"
 * )
 */
class GroupNodeRequest extends Constraint {

  /**
   * The message to show when a node is already a member of the group.
   *
   * @var string
   */
  public $message = 'Node "@title" is already a member of group';

}
