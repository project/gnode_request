<?php

namespace Drupal\gnode_request\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Checks user membership.
 */
class GroupNodeRequestValidator extends ConstraintValidator {

  /**
   * Type-hinting in parent Symfony class is off, let's fix that.
   *
   * @var \Symfony\Component\Validator\Context\ExecutionContextInterface
   */
  protected $context;

  /**
   * {@inheritdoc}
   */
  public function validate($group_content, Constraint $constraint) {

    /** @var \Drupal\group\Entity\GroupContentInterface $group_content */
    /** @var \Drupal\grequest\Plugin\Validation\Constraint\GroupMembershipRequest $constraint */

    // Apply logic only to group request membership group content.
    if ($group_content->getContentPlugin()->getBaseId() !== 'group_node_request') {
      return;
    }

    // Only run our checks if a group was referenced.
    if (!$group = $group_content->getGroup()) {
      return;
    }

    // Only run our checks if an entity was referenced.
    $entity = $group_content->getEntity();
    if (empty($entity)) {
      return;
    }

    if ($group->getContentByEntityId('group_node:' . $entity->getType(), $entity->id())) {
      $this->context->addViolation($constraint->message, [
        '@title' => $entity->label(),
      ]);
    }

  }

}
